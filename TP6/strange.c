#include <stdio.h>
#include <stdlib.h>

int slen(char *s){
  int l = 0;
  char *p; 
  p = s;
  while (*p++) {
    l++;
  }
  return l;
}

int is_upper(char *s){
  int upper = -1;
  while (*s) {
    if (*s >= 'a' && *s <= 'z') {
      upper = 0;
      break;
    }
    s++;
  }
  return upper;
}

int is_lower(char *s){
  int lower = -1;
  while (*s) {
    if (*s >= 'A' && *s <= 'Z'){
      lower = 0;
      break;
    }
    s++;
  }
  return lower;
}

void supper(char *s){
  while (*s){
    if (*s >= 'a' && *s <= 'z') {
      *s -= 32;
    }
  s++;
  }
  
}

int sequal(char *s1, char *s2){
  char *p, *q;
  int equal = -1;
  p = s1;
  q = s2;

  while (*p != '\0' && *q != '\0' && *p == *q) {
    p++;
    q++;
  }
  return *p == *q;
}


int main() {
  char s1[] = "Hello World!";
  char s2[] = "BONJOUR";
  char s3[] = "bonsoir";
  char s4[] = "Bon";
  char s5[] = "antidote";
  char s6[] = "antidote";

  printf("Longueur de \"%s\" : %d\n", s1, slen(s1));
  printf("La string \"%s\" est : %d\n", s2, is_upper(s2));
  printf("La string \"%s\" est : %d\n", s2, is_lower(s2)); 
  printf("La string \"%s\" est : %d\n", s3, is_lower(s3));
  printf(s3);
  printf("\n");
  supper(s3);
  printf(" \"%s\"", s3);
  
  printf("Les string \"%s\" et \"%s\" sont %d\n" , s5, s6, sequal(s5, s6));
  
}
