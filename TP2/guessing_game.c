#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void main(){
   srand(time(NULL));
   int target = rand() % 101;
   int guess;
   while (guess != target){
       printf("Donnez un nombre\n > ");
       scanf("%d", &guess);
       if (guess > target){
           printf("C'est moins !\n");
       }
       else if (guess < target){
           printf("C'est plus !\n");
           }
   }
   printf("Bravo !\n");
}

