#include<stdio.h>
#include<stdlib.h>

// double au lieu de float (plus précis)

double add(double x, double y) {
    return x + y;
}

double sub(double x, double y) {
    return x - y;
}

double mul(double x, double y) {
    return x * y;
}




// divv pour ne pas avoir de conflit avec div 
// défini dans stdlib.h
double divv(double x, double y) {
    return x / y;
}

int calc(double a, char op, double b) {
    double res;
    //char op;

    // %f pour un float, %lf pour un double
    printf("Calcul de : %.2lf %c %.2lf\n", a, op, b);

    switch (op) {
        case '+':
           res = add(a, b);
           break;
        case '-':
           res = sub(a, b);
           break;
        case '*':
           res = mul(a, b);
           break;
        case '/':
           res = divv(a, b);
           break;
        default:
           printf("Opération inconnue.\n");
           exit(EXIT_FAILURE);
    } 
    printf("Résultat : %.2lf\n", res);
    return 0;
}

int main(int argc, char *argv[]) {
  double term1 = strtold(argv[1], NULL);
  
  double term2 = strtold(argv[3], NULL);
  
  calc(term1, *argv[2], term2);
}
