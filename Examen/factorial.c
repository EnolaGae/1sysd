#include <stdio.h>
#include <stdlib.h>

int factorial(int n){
    int total = 1;
    int i = 1;
    
    while (i != n+1){
    total = total * i;
    printf("%d! = %d\n",i, total);
    i++;
  }  
  return total;
  }

int main(int argc, char *argv[]) {
  if (argc == 2 && argv[1] != NULL) {
    int convert = atoi(argv[1]);
    if (convert == 0){
      printf("%d\n",1);
      return 0;
    }
    factorial(convert);
    return 0;
  }
}
