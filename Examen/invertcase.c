#include <stdio.h>

void invertcase(char *s) {
  while (s != NULL) {
    if (*s >= 'a' && *s <= 'z'){
      *s -= 32;
    }
    else if (*s >= 'A'&& *s <= 'Z'){
      *s += 32;
    }
  s++;
  }
}

void main(int argc, char *argv[]) {
  if (&argv[1] != NULL) {
    for (int i = 1; argv[i] != NULL; i++) {
      invertcase(argv[i]);
      printf(" %s", argv[i]);
    }
  }
  printf("\n");
}
