#include <stdio.h>
#include <stdlib.h>

float pi = 3.14159;

float p(float r) {
  return (2 * pi * r);
}

float s(float r) {
  return (((pi * r) * r) );
}

int  main(){
  float r;
  printf("veuillez rentrer le rayon d'un cercle.\n > ");
  scanf("%f", &r);
  printf("Périmètre du cercle : %.3f\nSurface du cercle : %.3f\n", p(r), s(r));
}

