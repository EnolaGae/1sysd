# Validation de vos dépôts GIT

Vous devriez avoir créé sur gitlab (voire github) un projet
public nommé 1sysd, public. Dans l'interface Web de gitlab
sous le bouton code vous devriez voir une url de la forme :

https://gitlab.com/votre_id/1sysd

Merci de me l'envoyer sur Wimi...

Dépôt public individuel : ok !!!

## Initialisation du TP

0. Vous devriez avoir déjà cloné votre dépôt avec l'url 
ssh (git@gitlab.com...) , si ce n'est pas le cas :

~~~~Bash
$ sudo apt install git
$ cd
$ pwd
/home/votre_id
$ ls
.... 1sysd ....
$ rm -rf 1sysd
$ git clone git@gitlab.com.../1sysd
$ ls 
... 1sysd ...
~~~~

_Note : Si vous avez un problème lors du git clone il est probable que
vous n'avez pas importé votre clef publique ssh dans votre profil
gitlab !_

1. Se placer dans le dépôt 1sysd :

~~~~Bash
$ pwd
/home/votre_id
$ cd 1sysd
$ pwd
/home/votre_id/1sysd
$ git status
... 
~~~~

# Mise en œuvre du workflow d'un premier TP

1. Crée un répertoire pour le TP et s'y placer :

~~~~Bash
$ mkdir TP0
$ cd TP0
$ pwd
/home/votre_id/1sysd/TP0
~~~~

2. Créer un premier fichier C avec un éditeur de votre choix :

- `gnome-text-editor`
- `gedit` : `sudo apt install gedit`
- `emacs` : `sudo apt install emacs`
- `vim` : `sudo apt install vim`
- `neovim` : `sudo apt install neovim`
- `codium` : ça peut s'installer ....
- ...

_Note_ : dans la suite j'indique `vi` comme éditeur, mais vous
utilisez celui que vous préférez :

~~~~Bash
$ vi hello.c
~~~~

Le source C à saisir et sauvegarder :

~~~~C
#include<stdio.h>

int main() {
    printf("Hello World!\n");
    return 0;
}
~~~~

On va le compiler et l'exécuter :

~~~~Bash
$ ls
hello.c
$ cc hello.c
$ ls
hello.c a.out
$ a.out
... : inconnu
$ ./a.out
Hello World!
$
~~~~

Nous pouvons maintenant "commiter" l'état de ce TP et le publier :

~~~~Bash
$ git add hello.c
$ git commit -a -m "first program"
$ git push
~~~~

Vérifiez sur la page Web du projet que le fichier et le commentaire
de commit est bien visible.

*Bravo : vous venez de réaliser un premier workflow professionnel !*

## Comment importer des fichiers de mon dépôt vers le votre ?

Si vous ne l'avez pas déjà fait, vous pouvez cloner mon dépôt à
la base de votre répertoire de connexion (surtout PAS dans votre
dépôt !!!) :

~~~~Bash
$ cd
$ pwd
/home/votre_id
$ git clone https://gitlab.com/python_431/1sysd-b1-cf23.git
$ ls
... 1sysd ... 1sysd-b1-cf23
$ cd 1sysd-b1-cf23
$ ls
LINUX.md GIT.md TP0
~~~~

Si mon dépot évolue (et il va évoluer !) vous pouvez mettre à jour
votre copie :

~~~~Bash
$ cd ~/1sysd-b1-cf23
$ git pull
$ cd TP0
$ ls
... hello.c ... bonjour.c
~~~~

Si vous retournez dans votre dépôt vous n'avez plus qu'à copier le
fichier et l'ajouter au suivi par git (notez le point "." à la fin
de la commande `cp`) :

~~~~Bash
$ cd ~/1sysd/TP0
$ cp ~/1sysd-b1-cf23/TP0/bonjour.c .
$ ls
... hello.c ... bonjour.c ...
$ cc bonjour.c
$ ./a.out
Bonjour le monde !
$ git add bonjour.c
$ git commit -a -m "ajout bonjour.c"
$ git push
~~~~

Et voilà le nouveau fichier est dans votre dépôt !

## Rappels des commandes GIT de base :

- `git add` : déclenche le suivi d'un ou plusieurs fichiers, à faire une
  seule fois par fichier concerné (à ne pas faire sur des binaires !)
- `git commit -a -m "bla bla"` : actualise l'historique avec l'état actuel des fichiers
  suivis
- `git rm fichier` : supprime le fichier de l'état actuel
- `git mv ... ...` : déplace/renomme un fichier suivi
- `git push` : envoie l'état actuel de l'historique dans le référentiel distant (remote)
- `git pull` : synchronise l'état actuel avec celui du référentiel distant
- git sait faire bien plus de choses, vous aurez l'occasion de le pratiquer

## `make` et Makefile

Le fichier Makefile ici permet à la commande `make` de piloter la constuction de
nos objets (programmes, etc.) à partir de nos sources :

~~~~Makefile
all: hello bonjour

hello: hello.c
	gcc -o hello hello.c

bonjour: bonjour.c
	gcc -o bonjour bonjour.c

hello.s: hello.c
	gcc -S hello.c

clean:
	rm -f hello bonjour *.s
~~~~

_Piège_ : les commandes à exécuter sont précédée d'un caractère TAB et non pas d'espaces...

Il indique :

1. La cible par défaut (si `make` est exécutée sans argument) est _all_
2. _all_ indique qu'il faut examiner les cibles _hello_ et _bonjour_
3. _hello_ dépend du source _hello.c_, et on l'obtient par `gcc -o hello hello.c`
4. idem pour _bonjour_
5. idem pour _hello.s_
6. _clean_ fait le ménage (supprime toutes les cibles qui sont de fichiers)

