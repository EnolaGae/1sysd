#include <stdio.h>
#include <stdlib.h>

float add(float a,float b){
    return a+b;
}

float sub(float a,float b){
    return a-b;
}

float mult(float a,float b){
    return a*b;
}

float divi(float a,float b){
    return a/b;
}

float main(){
   float a;
   float b;
   char calc;
   float res;
  
   printf("Premier nombre: ");
   scanf("%f", &a);

   printf("Deuxième nombre: ");
   scanf("%f", &b);

   printf("Opérateur: ");
   scanf (" %c", &calc);

   switch(calc){
       case '+':
          res =  add(a,b);
          printf("%.2f\n", res);
          break;
       case '-':
           res = sub(a,b);
           printf("%.2f\n", res);
           break;
       case '*':
           res= mult(a,b);
           printf("%.2f\n", res);
           break;
       case '/':
           res = divi(a,b);
           printf("%.2f\n", res);
           break;
      }
   
}


