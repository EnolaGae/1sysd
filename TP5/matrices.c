#include<stdio.h>

#define DIM 3

void  print_matrice(long double M[DIM][DIM]) {
    for  (int i = 0; i < DIM; i++) {
        for (int j = 0; j < DIM; j++) {
            printf("%2.2LF ", M[i][j]);
        }
        printf("\n");
    }
}

void add_matrice(long double A[DIM][DIM], long double B[DIM][DIM],
    long double R[DIM][DIM]) {
    // mettre dans R les termes du produit matriciel de A et B
    for (int i = 0; i < DIM; i++) {
        for (int j = 0; j < DIM; j++) {
            R[i][j] = A[i][j] * B[i][j];
        R[i][j] = A[i][j] * B[i][j];
}}}
void mul_matrice(long double A[DIM][DIM], long double B[DIM][DIM],
        long double R[DIM][DIM]) {
    // mettre dans R la somme des éléments de A et B
    for (int i = 0; i < DIM; i++) {
        for (int j = 0; j < DIM; j++) {
            R[i][j] = 0;
            for (int k = 0; k < DIM; k++) {
                R[i][j] += A[i][k] * B[k][j];
            }
        }
   }
}


int main() {
    long double A[DIM][DIM] = { { 1.0 , 2.0 , 3.0 }, 
                                { 4.0 , 5.0 , 6.0 },
                                { 7.0 , 8.0 , 9.0 }};
    long double B[DIM][DIM] = { { 2.0 , 3.0 , 4 },
                                { 5.0 , 6.0 , 7 },
                                { 8.0 , 9.0 , 10.0 } };
    long double SUM[DIM][DIM];
    long double PROD[DIM][DIM];
    print_matrice(A);
    printf("\n");
    print_matrice(B);
    add_matrice(A, B, SUM);
    printf("\n");
    print_matrice(SUM);
    mul_matrice(A, B, PROD);
    printf("\n");
    print_matrice(PROD);

    return 0;
}

