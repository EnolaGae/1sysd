#include <stdio.h> 
#include <stdlib.h>

#define MAXSIZE 5

int main() {
    int tab[MAXSIZE];
    int n, sum = 0;

    printf("Nombre d'entiers à saisir : ");
    scanf("%d", &n);
    if (n > MAXSIZE) {
        printf("Maximum %d. Exiting.\n", MAXSIZE);
        exit(EXIT_FAILURE);
    } 
    for (int i = 0; i < n; i++){
        printf("Valeur %d : ", i);
        scanf("%d", &tab[i]);
    }
    for (int i = 0; i < n; i++) {
        printf("%d ", tab[i]);
    }
    printf("\n");
}
